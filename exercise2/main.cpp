//
// Created by Orlando Leombruni on 21/04/2018.
//

#include <condition_variable>
#include <iostream>
#include <thread>
#include <deque>
#include <vector>
#include <numeric>
#include <random>
#include <iomanip>
#include <sstream>
#include <chrono>

#include "prime_calculation.h"
#include "waste_util.h"
#include "synchroqueue.h"
#include "channel.h"

class Collector {
private:
    SynchroQueue<std::pair<int, unsigned int>*>& q;
    int* array;
    size_t arrSz;
    long timeToWaste;

public:
    Collector(SynchroQueue<std::pair<int, unsigned int>*>& queue, int* resultsArray, size_t arraySize, long toW)
            : q(queue), array(resultsArray), arrSz(arraySize), timeToWaste(toW) {

    }

    void end() {
        q.push(nullptr);
    }

    void run() {
        std::pair<int, unsigned int>* pair = q.pop();
        while (pair != nullptr) {
            if (pair->first > arrSz || pair->first < 0) throw std::logic_error("invalid position");
            else {
                array[pair->first] = pair->second;
                delete pair;
                pair = q.pop();
            }
        }
    }
};

class Emitter {
private:
    std::vector<Channel<std::pair<int, unsigned int>>>& channels;
    unsigned int* taskset;
    size_t tasksetSz;
    long timeToWaste;

public:
    Emitter(std::vector<Channel<std::pair<int, unsigned int>>>& ch, unsigned int* ts, size_t tSz, long toW)
            : channels(ch), taskset(ts), tasksetSz(tSz), timeToWaste(toW){

    };


    void run() {
        int i{0};
        while (i < tasksetSz) {
            for (unsigned int j{0}; j < channels.size(); ++j) {
                auto& channel{channels.at(j)};
                if (channel.isConsumed()) {
                    std::cout << "Entering " << j << std::endl;
                    int x = (int)Waste::waste(timeToWaste, 1.0);
                    auto t = std::pair<int, unsigned int>(i, taskset[i]+x);

                    std::cout << "sending (" << t.first << ", " << t.second << ") to " << j << std::endl;

                    channel.produce(t);
                    i++;
                } else {
                    std::cout << "waiting " << j << std::endl;
                }
            }
        }

        for (auto &channel: channels) {
            channel.close();
        }

    }
};

class Worker {
private:
    Channel<std::pair<int, unsigned int>>& ch;
    SynchroQueue<std::pair<int, unsigned int>*>& q;

public:
    Worker(Channel<std::pair<int, unsigned int>>& channel, SynchroQueue<std::pair<int, unsigned int>*>& queue)
            : ch(channel), q(queue) {

    }

    void run() {
        std::pair<int, unsigned int> *s = ch.consume();
        std::pair<int, unsigned int> *p = nullptr;
        while (s != nullptr) {
            unsigned int toCalc = s->second;
            int position = s->first;

            std::vector<unsigned int> vec = eratosthenes(toCalc);

            auto toReturn = std::accumulate(vec.begin(), vec.end(), static_cast<unsigned int>(0));

            p = new std::pair<int, unsigned int>;
            p->first = position;
            p->second = toReturn;
            q.push(std::ref(p));
            delete s;
            s = ch.consume();
        }
        std::cout << "closed" << std::endl;
        //q.push(nullptr);
    }
};

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cout << "Please enter the array size\nUsage: " << argv[0] << " array_size [number_of_workers] [delay]" << std::endl;

        return 1;
    }

    unsigned int nthreads;
    long delay;

    if (argc > 2) {
        std::istringstream iss{argv[2]};

        if (!(iss >> nthreads)) {
            std::cout << "Please the number of workers correctly\nUsage: " << argv[0] << " array_size [number_of_workers] [delay]" << std::endl;

            return 1;
        }

        if (argc > 3) {
            std::istringstream iss2{argv[3]};

            if (!(iss2 >> delay)) {
                std::cout << "Please enter the delay correctly\nUsage: " << argv[0] << " array_size [number_of_workers] [delay]" << std::endl;

                return 1;
            }
        } else delay = Waste::small;
    } else {
        nthreads = std::thread::hardware_concurrency();
        delay = Waste::small;
    }

    size_t arrSize;

    std::istringstream iss{argv[1]};

    if (!(iss >> arrSize)) {
        std::cout << "Please enter the array size\nUsage: " << argv[0] << " array_size [number_of_workers][delay]" << std::endl;

        return 1;
    }

    SynchroQueue<std::pair<int, unsigned int>*> q;
    unsigned int arr[arrSize];
    int arr2[arrSize];

    std::random_device r;
    std::mt19937 engine(r());
    std::uniform_int_distribution<unsigned int> distr(1, 10000);

    auto start = std::chrono::high_resolution_clock::now();

    for (unsigned int i{0}; i < arrSize; ++i) {
        arr[i] = distr(engine);
        arr2[i] = -1;
    }

    std::vector<Channel<std::pair<int, unsigned int>>> channels(nthreads);
    std::vector<std::thread> threads;

    Collector c(q, arr2, arrSize, delay);
    Emitter e(channels, arr, arrSize, delay);

    std::thread p(&Emitter::run, std::ref(e));
    for (size_t i{0}; i < nthreads; ++i) {
        Worker w{channels.at(i), q};
        threads.emplace_back(std::thread(&Worker::run, std::ref(w)));
    }
    std::thread coll(&Collector::run, std::ref(c));

    p.join();
    for (auto& th: threads) {
        th.join();
    }
    c.end();
    coll.join();

    auto end = std::chrono::high_resolution_clock::now();

    std::cout << std::setw(7) << "Index " << std::setw(7) << "Number" << std::setw(7) << "Primes" << std::endl;
    for (int i{0}; i < arrSize; ++i) {
        std::cout << std::setw(7) << i << std::setw(7) << arr[i] << std::setw(7) << arr2[i] << std::endl;
    }

    std::cout << "spent " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
              << " ms computing " << arrSize << " elements with " << nthreads << " threads "
                 "and a delay of " << delay << " iterations." << std::endl;
}