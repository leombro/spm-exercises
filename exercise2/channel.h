//
// Created by Orlando Leombruni on 23/04/2018.
//

#ifndef CHANNEL_H
#define CHANNEL_H

#include <condition_variable>

template <typename T>
class Channel {
private:
    std::condition_variable cond;
    std::mutex m;
    T* elem = nullptr;
    bool produced{false};
    bool consumed{true};
    bool closed{false};

public:
    bool isConsumed() {
        std::unique_lock<std::mutex> lock(m);
        bool toRet = consumed;
        lock.unlock();
        return toRet;
    }

    void produce(T& item) {
        std::unique_lock<std::mutex> lock(m);
        cond.wait(lock, [=]{ return consumed; });
        elem = new T;
        *elem = item;
        produced = true;
        consumed = false;
        lock.unlock();
        cond.notify_one();
    }

    T* consume() {
        std::unique_lock<std::mutex> lock(m);
        cond.wait(lock, [=]{ return closed || produced; });
        T* item;
        if (!closed || (closed && produced)) {
            item = elem;
        }
        else item = nullptr;
        consumed = true;
        produced = false;
        lock.unlock();
        cond.notify_one();
        return item;
    }

    void close() {
        std::unique_lock<std::mutex> lock(m);
        closed = true;
        lock.unlock();
    }

};
#endif //CHANNEL_H
