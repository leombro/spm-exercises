//
// Created by Orlando Leombruni on 21/04/2018.
//

#include <cmath>
#include <vector>
#include "prime_calculation.h"

bool isPrime(unsigned int n) {
    if (n < 2) return false;
    if (n == 2) return true;
    if (n % 2 == 0) return false;
    auto square = int(std::ceil(std::sqrt(n)));
    for (int i{3}; i <= square; i += 2) {
        if (n % i == 0) return false;
    }
    return true;
}

std::vector<unsigned int> eratosthenes(unsigned int n) {
    std::vector<unsigned int> b(n+1);

    for (int i{0}; i <= n; ++i) {
        b[i] = 1;
    }

    b[0] = 0;
    b[1] = 0;
    auto square = int(std::ceil(std::sqrt(n)));
    for (int i{2}; i <= square; ++i) {
        if (b[i]) {
            for (int j{2*i}; j <= n; j += i) {
                b[j] = 0;
            }
        }
    }

    return b;
}