//
// Created by Orlando Leombruni on 21/04/2018.
//

#ifndef PRIME_CALCULATION_H
#define PRIME_CALCULATION_H

bool isPrime(unsigned int);

std::vector<unsigned int> eratosthenes(unsigned int);

#endif //PRIME_CALCULATION_H
