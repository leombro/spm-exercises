//
// Created by Orlando Leombruni on 23/04/2018.
//

#ifndef SYNCHROQUEUE_H
#define SYNCHROQUEUE_H

#include <condition_variable>
#include <deque>

template <typename T>
class SynchroQueue {
private:
    std::condition_variable cond;
    std::mutex m;
    std::deque<T> deque;

public:

    void push(const T& item) {
        std::unique_lock<std::mutex> lock(m);
        deque.push_front(item);
        lock.unlock();
        cond.notify_one();
    }

    T pop() {
        std::unique_lock<std::mutex> lock(m);
        cond.wait(lock, [=]{ return !deque.empty(); });
        auto item{std::move(deque.back())};
        deque.pop_back();
        lock.unlock();
        return item;
    }
};

#endif //SYNCHROQUEUE_H
