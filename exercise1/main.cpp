#include <iostream>
#include <iomanip>
#include <sstream>

#include "orchestrator.h"
#include "waste_util.h"

int main(int argc, char* argv[]) {

    int vectorSize, nVectors;

    if (argc < 3) {
        std::cout << "Please insert the number and size of vectors.\n"
                  << "Usage: " << argv[0] << " n_vect vect_size" << std::endl;
        return -1;
    }

    std::istringstream iss1{argv[1]}, iss2{argv[2]};

    if (!(iss1 >> nVectors && iss2 >> vectorSize)) {
        std::cout << "Please insert the number and size of vectors.\n"
                  << "Usage: " << argv[0] << " n_vect vect_size" << std::endl;
        return -1;
    }

    // +++++++++ BALANCED +++++++++

    std::cout << "\n\n\t\tBalanced computation:" << std::endl;

    // Sequential balanced version
    std::cout << "\nSEQUENTIAL" << std::endl;
    auto usec_seq_b{execute(Computation::Sequential, nVectors, vectorSize, Waste::small, Waste::small)};

    // Single thread balanced version
    std::cout << "\nONE THREAD" << std::endl;
    auto usec_sthread_b{execute(Computation::SingleThread, nVectors, vectorSize, Waste::small, Waste::small)};

    // Balanced pipeline version
    std::cout << "\nPIPELINE" << std::endl;
    auto usec_pipe_b{execute(Computation::Pipeline, nVectors, vectorSize, Waste::small, Waste::small)};


    // +++++++++ UNBALANCED +++++++++

    std::cout << "\n\n\t\tUnbalanced computation:" << std::endl;
    // Sequential unbalanced version
    std::cout << "\nSEQUENTIAL" << std::endl;
    auto usec_seq_u{execute(Computation::Sequential, nVectors, vectorSize, Waste::small, Waste::big)};

    // Single thread unbalanced version
    std::cout << "\nONE THREAD" << std::endl;
    auto usec_sthread_u{execute(Computation::SingleThread, nVectors, vectorSize, Waste::small, Waste::big)};

    // Unbalanced pipeline version
    std::cout << "\nPIPELINE" << std::endl;
    auto usec_pipe_u{execute(Computation::Pipeline, nVectors, vectorSize, Waste::small, Waste::big)};

    std::cout << "\n\n=================+ BALANCED STAGES +=================" << std::endl;
    std::cout << "SEQUENTIAL: Spent " << usec_seq_b << " usecs to compute a stream of " << nVectors << " vectors of " << vectorSize << " numbers" << std::endl;
    std::cout << "ONE THREAD: Spent " << usec_sthread_b << " usecs to compute a stream of " << nVectors << " vectors of " << vectorSize << " numbers" << std::endl;
    std::cout << "PIPELINE: Spent " << usec_pipe_b << " usecs to compute a stream of " << nVectors << " vectors of " << vectorSize << " numbers" << std::endl;
    std::cout << "Scalability is " << (static_cast<double>(usec_sthread_b) / usec_pipe_b) << std::endl;
    std::cout << "Speedup is " << (static_cast<double>(usec_seq_b) / usec_pipe_b) << std::endl;
    std::cout << "Efficiency is " << ((static_cast<double>(usec_seq_b)/4) / usec_pipe_b) << std::endl;

    std::cout << "\n\n================+ UNBALANCED STAGES +================" << std::endl;
    std::cout << "SEQUENTIAL: Spent " << usec_seq_u << " usecs to compute a stream of " << nVectors << " vectors of " << vectorSize << " numbers" << std::endl;
    std::cout << "ONE THREAD: Spent " << usec_sthread_u << " usecs to compute a stream of " << nVectors << " vectors of " << vectorSize << " numbers" << std::endl;
    std::cout << "PIPELINE: Spent " << usec_pipe_u << " usecs to compute a stream of " << nVectors << " vectors of " << vectorSize << " numbers" << std::endl;
    std::cout << "Scalability is " << (static_cast<double>(usec_sthread_u) / usec_pipe_u) << std::endl;
    std::cout << "Speedup is " << (static_cast<double>(usec_seq_u) / usec_pipe_u) << std::endl;
    std::cout << "Efficiency is " << ((static_cast<double>(usec_seq_u)/4) / usec_pipe_u) << std::endl;

    return 0;
}
