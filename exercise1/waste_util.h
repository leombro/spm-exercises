//
// Created by Orlando Leombruni on 05/04/2018.
//

#ifndef WASTE_UTIL_H
#define WASTE_UTIL_H

namespace Waste {

    const long zero{0};
    const long small{64*1024};
    const long big{8*1024*1024};

    extern double waste(long, double);
}

#endif //WASTE_UTIL_H
