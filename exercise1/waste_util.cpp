//
// Created by Orlando Leombruni on 05/04/2018.
//

#include <cmath>
#include "waste_util.h"

double Waste::waste(long iter, double x) {
    if (0 == iter) return 0;
    for (long i{0}; i < iter; ++i) {
        x = sin(x);
    }
    return x;
}