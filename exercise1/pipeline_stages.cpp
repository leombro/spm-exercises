//
// Created by Orlando Leombruni on 05/04/2018.
//

#include <random>
#include <sstream>
#include <iomanip>
#include <iostream>

#include "pipeline_stages.h"

// Pipeline initial stage (stream generation)
void stage1(int nVectors, int vectorSize, SynchroQueue<std::vector<double>>& queue) {
    // RNG initialization
    std::random_device r;
    std::mt19937 engine(r());
    std::uniform_real_distribution<double> distr(0.0, 1.0);

    for (int i{0}; i < nVectors; ++i) {
        std::vector<double> g;
        for (int j{0}; j < vectorSize; ++j) {
            g.push_back(distr(engine));
        }
        queue.push(std::move(g));
    }
    queue.terminate();
}

// Pipeline middle stage(s) (stream transformation)
void stage2(double f(double, long), long toWaste,
            SynchroQueue<std::vector<double>>& inQueue,
            SynchroQueue<std::vector<double>>& outQueue) {
    while (!inQueue.isTerminated()) {
        std::vector<double> v{inQueue.pop()};

        for (auto& i: v) {
            i = f(i, toWaste);
        }

        outQueue.push(std::move(v));
    }

    outQueue.terminate();
}

// Pipeline end stage (stream printing)
void stage4(SynchroQueue<std::vector<double>>& inQueue) {
    int count{1};
    while (!inQueue.isTerminated()) {
        std::stringstream s;
        s << "Vector #" << std::setw(2) << count << ": [ ";
        std::vector<double> v{inQueue.pop()};
        for (const auto& i: v) {
            s << i << " ";
        }
        s << "]";
        std::cout << s.str() << std::endl;
        count++;
    }
}
