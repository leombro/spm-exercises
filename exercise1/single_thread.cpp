//
// Created by Orlando Leombruni on 05/04/2018.
//

#include <random>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

#include "synchroqueue.h"
#include "single_thread.h"
#include "waste_util.h"

// "Pipeline-like" single node computation
void singleStage(int nVectors, int vectorSize, long toWaste1, long toWaste2) {

    // Stage 1
    SynchroQueue<std::vector<double>> s_1_2;
    std::random_device r;
    std::mt19937 engine(r());
    std::uniform_real_distribution<double> distr(0.0, 1.0);

    for (int i{0}; i < nVectors; ++i) {
        std::vector<double> g;
        for (int j{0}; j < vectorSize; ++j) {
            g.push_back(distr(engine));
        }
        s_1_2.push(std::move(g));
    }
    s_1_2.terminate();

    // Stage 2
    SynchroQueue<std::vector<double>> s_2_3;

    while (!s_1_2.isTerminated()) {
        std::vector<double> v{s_1_2.pop()};
        for (auto& i: v) {
            i = i + 1 + Waste::waste(toWaste1, i);
        }
        s_2_3.push(std::move(v));
    }
    s_2_3.terminate();

    // Stage 3
    SynchroQueue<std::vector<double>> s_3_4;

    while (!s_2_3.isTerminated()) {
        std::vector<double> v{s_2_3.pop()};
        for (auto& i: v) {
            i = i * 2 + Waste::waste(toWaste2, i);
        }
        s_3_4.push(std::move(v));
    }
    s_3_4.terminate();

    // Stage 4
    int count{1};

    while (!s_3_4.isTerminated()) {
        std::stringstream s;
        s << "Vector #" << std::setw(2) << count << ": [ ";
        std::vector<double> v{s_3_4.pop()};
        for (const auto& i: v) {
            s << i << " ";
        }
        s << "]";
        std::cout << s.str() << std::endl;
        count++;
    }
}