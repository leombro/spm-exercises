//
// Created by Orlando Leombruni on 05/04/2018.
//

#ifndef PIPELINE_STAGES_H
#define PIPELINE_STAGES_H

#include <vector>

#include "synchroqueue.h"

void stage1(int, int, SynchroQueue<std::vector<double>>&);
void stage2(double (double, long), long,
            SynchroQueue<std::vector<double>>&,
            SynchroQueue<std::vector<double>>&);
void stage4(SynchroQueue<std::vector<double>>&);

#endif //PIPELINE_STAGES_H
