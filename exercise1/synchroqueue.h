//
// Created by Orlando Leombruni on 05/04/2018.
//

#ifndef SYNCHROQUEUE_H
#define SYNCHROQUEUE_H

#include <mutex>
#include <deque>
#include <condition_variable>

template <typename T>
class SynchroQueue {
public:
    T pop() {
        std::unique_lock<std::mutex> lock(mut);
        cond.wait(lock, [=]{ return !mQueue.empty(); });
        auto item{std::move(mQueue.back())};
        mQueue.pop_back();
        return item;
    }

    void push(T&& item) {
        std::unique_lock<std::mutex> lock(mut);
        mQueue.push_front(item);
        lock.unlock();
        cond.notify_one();
    }

    void terminate() {
        std::unique_lock<std::mutex> lock(mut);
        termination = true;
        lock.unlock();
    }

    bool isTerminated() {
        std::unique_lock<std::mutex> lock(mut);
        bool isTerminated = termination && mQueue.empty();
        lock.unlock();
        return isTerminated;
    }
private:
    std::deque<T> mQueue;
    std::mutex mut;
    std::condition_variable cond;
    bool termination{false};
};

#endif //SYNCHROQUEUE_H
