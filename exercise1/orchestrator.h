//
// Created by Orlando Leombruni on 05/04/2018.
//

#ifndef ORCHESTRATOR_H
#define ORCHESTRATOR_H

enum class Computation {
    Sequential,
    SingleThread,
    Pipeline
};

long long int execute(Computation, int, int, long, long);

#endif //ORCHESTRATOR_H
