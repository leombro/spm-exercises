Preliminary considerations
--------------------------

For what concerns a pipeline with balanced computation stages, a speedup of
around 2 is expected since the "*source*" and "*sink*" stages should contribute
next to nothing in terms of elapsed time. Conversely, we expect the speedup to
decrease when the stages become unbalanced -- the bigger the unbalance, the
sharper the decrease, since the computation becomes almost "sequential" in the
sense that the bulk of the time is spent in one stage.

Results
-------

With this simple computation, the effects of the pipeline pattern are completely
overshadowed by the overheads. To obtain a more coherent result, the computation
time has been increased artificially using the same `waste` function needed to
unbalance the stages.

With this trick, the observed behavior is similar to the expected one: both
speedup and scalability are around 2, while efficiency is near 0.5. The higher
the number of vectors, the better the performance of the pipeline version w.r.t.
the sequential one.

The unbalanced computation, as expected, presents scalability and speedup around
1, with efficiency near 0.25.