//
// Created by Orlando Leombruni on 05/04/2018.
//

#include <thread>
#include <chrono>

#include "orchestrator.h"
#include "waste_util.h"
#include "synchroqueue.h"
#include "pipeline_stages.h"
#include "sequential.h"
#include "single_thread.h"

//Pipeline computation
void pipeline(int nVectors, int vectorSize, long toWaste1, long toWaste2) {
    SynchroQueue<std::vector<double>>
            stage_1_2,
            stage_2_3,
            stage_3_4;

    auto plusone = [](double i, long toWaste) {
        return i + 1 + Waste::waste(toWaste, i);
    };

    auto doubleit = [](double i, long toWaste) {
        return i * 2 + Waste::waste(toWaste, i);
    };

    std::thread t1(stage1, nVectors, vectorSize, std::ref(stage_1_2));
    std::thread t2{stage2, plusone, toWaste1, std::ref(stage_1_2), std::ref(stage_2_3)};
    std::thread t3{stage2, doubleit, toWaste2, std::ref(stage_2_3), std::ref(stage_3_4)};
    std::thread t4{stage4, std::ref(stage_3_4)};

    t1.join();
    t2.join();
    t3.join();
    t4.join();
}

long long int execute(Computation c, int noVectors, int vectorsSize, long toWaste1, long toWaste2) {
    auto start{std::chrono::high_resolution_clock::now()};

    switch (c) {
        case Computation::Sequential:
            sequential(noVectors, vectorsSize, toWaste1, toWaste2);
            break;
        case Computation::SingleThread:
            std::thread(singleStage, noVectors, vectorsSize, toWaste1, toWaste2).join();
            break;
        case Computation::Pipeline:
            pipeline(noVectors, vectorsSize, toWaste1, toWaste2);
            break;
    }

    auto end{std::chrono::high_resolution_clock::now()};

    return std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
}