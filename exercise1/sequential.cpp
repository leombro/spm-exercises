//
// Created by Orlando Leombruni on 05/04/2018.
//

#include "sequential.h"

#include <random>
#include <vector>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "waste_util.h"

//"Sequential" computation
void sequential(int nVectors, int vectorSize, long toWaste1, long toWaste2) {
    {
        std::random_device r;
        std::mt19937 engine(r());
        std::uniform_real_distribution<double> distr(0.0, 1.0);

        for (int i{0}; i < nVectors; ++i) {
            std::vector<double> g;
            for (int j{0}; j < vectorSize; ++j) {
                g.push_back(distr(engine));
            }


            std::stringstream s;
            s << "Vector #" << std::setw(2) << (i+1) << ": [ ";
            for (auto& d: g) {
                d = d + 1 + Waste::waste(toWaste1, d);
                d = d * 2 + Waste::waste(toWaste2, d);
                s << d << " ";
            }
            s << "]";
            std::cout << s.str() << std::endl;
        }
    }
}