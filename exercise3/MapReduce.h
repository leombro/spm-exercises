//
// Created by Orlando Leombruni on 04/05/2018.
//

#ifndef MAPREDUCE_H
#define MAPREDUCE_H

#include <utility>
#include <string>
#include <fstream>
#include <vector>
#include <thread>
#include <map>
#include <sstream>
#include <iostream>
#include <functional>

#include "synchroqueue.h"

/*
 * Utility class to implement a sort of "multi-map", where if a key gets inserted multiple times with different
 * values, the latter are stored into an array. The insertion can be done by multiple workers and is thus a mutually
 * exclusive operation, while the underlying map can be directly accessed by the single collector.
 */
template <typename K, typename V>
class ResultsContainer {
private:
    std::mutex m;

public:
    std::map<K, std::vector<V>> map;

    void insert(const std::pair<K, V>& p) {
        std::unique_lock<std::mutex> lock(m);
        if (map.count(p.first)) {
            map[p.first].push_back(p.second);
        } else {
            map.insert(std::make_pair(p.first, std::vector<V>{p.second}));
        }
        lock.unlock();
    }

};

/*
 * The class that implements the MapReduce pattern.
 *
 * The constructor of the class creates a thread pool and opens both input and output files. Then, the user
 * sets both the mapper and reducer function before explicitly starting the computation.
 */

template <typename Tkey, typename Tvalue>
class MapReduce {
private:
    bool setupMap{false}, setupReduce{false};
    std::function<std::vector<std::pair<Tkey, Tvalue>>(std::string)> map;
    std::function<Tvalue(Tvalue, Tvalue)> reduce;


    std::ifstream inFile;
    std::ofstream outFile;
    std::vector<std::thread> threadPool;
    SynchroQueue<std::string*> queue;
    ResultsContainer<Tkey, Tvalue> results;
    int nthreads;

    /*
     * Implementation of the workers' functionalities.
     *
     * Workers first build an internal partial result by applying the map function to each string extracted
     * by the producer-consumer queue and then applying the reduce function to the result obtained by the map.
     * At the end, the worker puts these partial results into the results queue.
     */
    void worker(int index) {
        int count{0};
        std::map<Tkey, std::vector<Tvalue>> mmap;
        std::string *current = queue.pop();

        while (current != nullptr) {
            std::string currStr = (*current);
            auto vec = map(currStr);
            for (const auto& pair: vec) {
                if (mmap.count(pair.first)) {
                    mmap[pair.first].push_back(pair.second);
                } else {
                    mmap.insert(std::make_pair(pair.first, std::vector<Tvalue>{pair.second}));
                }
            }
            delete current;
            current = queue.pop();
            count++;
        }

        for (const auto& pairs: mmap) {
            Tvalue res;
            if (pairs.second.size() > 1) {
                res = reduce(pairs.second.at(0), pairs.second.at(1));
                for (int i{2}; i < pairs.second.size(); ++i) {
                    res = reduce(res, pairs.second.at(i));
                }
            } else {
                res = pairs.second.at(0);
            }
            results.insert(std::make_pair(pairs.first, res));
        }

    }

public:
    MapReduce(const std::string& input, const std::string& output, int nthr): inFile(input), outFile(output), nthreads(nthr) {
        for (int i{0}; i < nthr; i++) {
            threadPool.emplace_back(std::thread(&MapReduce::worker, this, i));
        }
    };


    void setMapper(std::function<std::vector<std::pair<Tkey, Tvalue>>(std::string)> mapfun) {
        map = mapfun;
        setupMap = true;
    }

    void setReduce(std::function<Tvalue(Tvalue, Tvalue)> reducefun) {
        reduce = reducefun;
        setupReduce = true;
    }

    /*
     * Starts the computation by reading the input file and putting the strings into the producer-consumer queue.
     * It then starts the workers and waits for their termination; afterwards, it performs the final reduction
     * step on the partial results, writing the final pairs into the output file.
     */
    void compute() {
        if (!setupMap || !setupReduce) return;
        std::string line;
        size_t curr{0};
        while (std::getline(inFile, line)) {
            auto currLine = new std::string(line);
            queue.push(currLine);
            curr++;
            if (curr == nthreads) curr = 0;
        }

        for (int j{0}; j < nthreads; ++j) {
            queue.push(nullptr);
        }

        inFile.close();

        for (auto& thread: threadPool) {
            thread.join();
        }

        int j{1};
        for (auto& pair: results.map) {
            Tvalue res;
            if (pair.second.size() > 1) {
                res = reduce(pair.second.at(0), pair.second.at(1));
                for (int i{2}; i < pair.second.size(); ++i) {
                    res = reduce(res, pair.second.at(i));
                }
            } else {
                res = pair.second.at(0);
            }

            outFile << pair.first << "," << res << std::endl;
        }

        outFile.close();
    }
};


#endif //MAPREDUCE_H
