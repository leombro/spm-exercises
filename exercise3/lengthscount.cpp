//
// Created by Orlando Leombruni on 05/05/2018.
//

/**
 * This program takes in input a text file and outputs the frequency of each word length. For example, if
 * the input is a file containing the following text:
 *
 *      "a me tre mele a te tre pere a lui otto pesche"
 *
 * the program outputs the following:
 *
 *      1,3
 *      2,2
 *      3,3
 *      4,3
 *      6,1
 *
 *  since the input contains three words of length 1, two words of length 2, and so on.
 */

#include <iostream>
#include <sstream>
#include <iterator>
#include <algorithm>
#include "MapReduce.h"
#include "waste_util.h"

int main(int argc, char* argv[]) {

    int nThreads;
    static long toWaste = 0;  // Change this value to increase the computation time for the map function

    if (argc < 3) {
        std::cout << "Must specify input and output file and (optionally) number of threads\n"
                     << "Usage: " << argv[0] << " inputfile outputfile [n_threads]" << std::endl;
        return 1;
    }
    std::istringstream in{argv[1]}, out{argv[2]};
    std::string filenameIn, filenameOut;
    if (!(in >> filenameIn)) {
        std::cout << "Invalid input filename\n"
                  << "Usage: " << argv[0] << " inputfile outputfile [n_threads]" << std::endl;
        return 1;
    }
    if (!(out >> filenameOut)) {
        std::cout << "Invalid output filename\n"
                  << "Usage: " << argv[0] << " inputfile outputfile [n_threads]" << std::endl;
        return 1;
    }

    if (argc > 3) {  // The number of threads is an optional parameter; if omitted, the program will use
                     // the maximum number of processing elements available
        std::istringstream nthr{argv[3]};
        if (!(nthr >> nThreads)) {
            std::cout << "Invalid number of threads\n"
                      << "Usage: " << argv[0] << " inputfile outputfile [n_threads]" << std::endl;
            return 1;
        }
        if (nThreads > std::thread::hardware_concurrency()) nThreads = std::thread::hardware_concurrency();
    } else {
        nThreads = std::thread::hardware_concurrency();
    }

    auto setup_start = std::chrono::high_resolution_clock::now();
    MapReduce<size_t, int> mapReduce{filenameIn, filenameOut, nThreads};

    auto mapper = [](std::string s) {
        std::istringstream iss{s};
        std::string word;
        std::vector<std::pair<size_t, int>> results;

        while((iss >> word)) {
            results.emplace_back(std::make_pair(word.size(), 1 + Waste::waste(toWaste, 1)));
        }

        return results;
    };

    auto reducer = [](int val1, int val2) {
        return val1 + val2;
    };

    mapReduce.setMapper(mapper);
    mapReduce.setReduce(reducer);

    auto setup_end = std::chrono::high_resolution_clock::now();

    auto setup_duration = std::chrono::duration_cast<std::chrono::microseconds>(setup_end - setup_start);

    auto start = std::chrono::high_resolution_clock::now();
    mapReduce.compute();
    auto end = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);

    std::ifstream wc_file(filenameIn), wc_file2(filenameIn);
    std::istream_iterator<std::string> wc_in{ wc_file }, wc_end;

    std::cout << "Spent " << setup_duration.count() << " microseconds building up data structures and threads." << std::endl;

    std::cout << "Spent " << duration.count() << " microseconds computing a text of " << std::distance(wc_in, wc_end)
              << " words and "
              << std::count(std::istreambuf_iterator<char>(wc_file2), std::istreambuf_iterator<char>(), '\n')
              << " lines with " << nThreads << " threads" << std::endl;

    wc_file.close();
    wc_file2.close();
    return 0;
}